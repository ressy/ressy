The bulk of my material on GitLab is stored in [a group account](https://gitlab.com/ressy-group) so that I can keep things organized by category.  That's mainly:

 * [My example repos](https://gitlab.com/ressy-group/examples), building self-contained examples for specific pieces of software.
 * [My "paper helper" repos](https://gitlab.com/ressy-group/paper-helpers), where I bundle code and metadata I gather up while following along with published research papers.

My code elsewhere:

 * <https://github.com/ressy>
 * <https://github.com/ShawHahnLab>
